/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.orm.sql;

import cn.jfast.framework.jdbc.annotation.Delete;
import cn.jfast.framework.jdbc.db.ConnectionFactory;
import cn.jfast.framework.jdbc.orm.Executor;
import cn.jfast.framework.jdbc.orm.SqlException;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.SQLException;

public class DeleteSql extends Executor {

    private Delete delete;

    public DeleteSql(Type[] paramTypes,
                     String[] paramNames,
                     Object[] args,
                     Delete delete,
                     Type returnType,
                     Method method) throws IllegalAccessException, SQLException, ClassNotFoundException, SqlException {
        this.method = method;
        this.paramNames = paramNames;
        this.paramTypes = paramTypes;
        this.args = args;
        this.delete = delete;
        this.returnType = returnType;
        conn = ConnectionFactory.getThreadLocalConnection();
        super.wrapParam();
    }

    @Override
    public String getSql() {
        return delete.sql() + " ";
    }

}
