/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.log;

import java.lang.reflect.Proxy;

public class Connection {

	private static java.sql.Connection conn;
	private static ConnectionLogger handler;

	public static java.sql.Connection getConnection(java.sql.Connection con) {
		handler = new ConnectionLogger(con);
		conn = (java.sql.Connection) Proxy.newProxyInstance(con.getClass()
				.getClassLoader(), new Class[]{java.sql.Connection.class}, handler);
		return conn;
	}

}
