/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import cn.jfast.framework.base.cache.Cache;
import cn.jfast.framework.base.cache.cacheimpl.ConcurrentMapCache;
import cn.jfast.framework.jdbc.info.Table;

/**
 * jdbc属性
 *
 * @author 泛泛o0之辈
 */
public class DBProp {

	public static Cache tableCache = new ConcurrentMapCache("table");
	public static String JDBC_URL = "";
	public static String JDBC_USER = "";
	public static String JDBC_PASSWORD = "";
	public static String JDBC_DRIVER_CLASS = "";
	public static int MAX_POOL_SIZE = 100;
	public static int MIN_POOL_SIZE = 10;
	public static int INITIAL_POOL_SIZE = 10;
	public static int MAX_IDLE_TIME = 20;
	public static int ACQUIRE_INCREMENT = 4;
	public static String DB_NAME = "";
	public static String DB_VERSION = "";
	public static String DB_DRIVER = "";
	
	public static String getJdbcUrl(){
    	if(DBProp.JDBC_URL.contains("?")){
    		return DBProp.JDBC_URL+"&generateSimpleParameterMetadata=true";
    	} else {
    		return DBProp.JDBC_URL+"?generateSimpleParameterMetadata=true";
    	}
    }
	
	public static Table getTable(String tableName){
		return tableCache.get(tableName.toLowerCase(),Table.class);
	}
}
