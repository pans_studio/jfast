/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * Druid数据源
 *
 * @author 泛泛o0之辈
 */
public class AlibabaDruidDataSource implements IDataSource{
	
	private DruidDataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public boolean init() {
		dataSource = new DruidDataSource();
		dataSource.setUrl(DBProp.getJdbcUrl());
		dataSource.setUsername(DBProp.JDBC_USER);
		dataSource.setPassword(DBProp.JDBC_PASSWORD);
		dataSource.setDriverClassName(DBProp.JDBC_DRIVER_CLASS);
		dataSource.setMaxActive(DBProp.MAX_POOL_SIZE);
		dataSource.setMinIdle(DBProp.MIN_POOL_SIZE);
		dataSource.setInitialSize(DBProp.INITIAL_POOL_SIZE);
		return true;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 */
	public boolean stop() {
		if (dataSource != null)
			dataSource.close();
		return true;
	}
}
