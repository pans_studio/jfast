/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.junit;

import cn.jfast.framework.web.api.ApiContext;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * Junit测试启动jfast上下文
 */
public class JfastJUnit4ClassRunner extends BlockJUnit4ClassRunner {

    private Class<?> klass;
    
    public Class<?> getKlass() {
		return klass;
	}

	public void setKlass(Class<?> klass) {
		this.klass = klass;
	}

	public JfastJUnit4ClassRunner(Class<?> klass) throws InitializationError {
        super(klass);
        this.klass = klass;
    }

    @Override
    protected Statement withBefores(FrameworkMethod method, Object target, Statement statement) {
        ApiContext.loadConfig();
        ApiContext.loadDBInfo();
        ApiContext.loadDaoCache();
        ApiContext.loadResourceCache();
        target = ApiContext.fillResource(target);
        return super.withBefores(method, target, statement);
    }
}
