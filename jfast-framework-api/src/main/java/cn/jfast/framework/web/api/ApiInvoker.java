/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.api;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.jfast.framework.base.util.*;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.web.validate.ParameterValidate;
import cn.jfast.framework.web.annotation.*;
import cn.jfast.framework.upload.FileRequestResolver;
import cn.jfast.framework.upload.UploadFile;
import cn.jfast.framework.web.kit.JsonDateValueProcessor;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.JSONUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Api 对应实际方法的分析与执行
 */
public class ApiInvoker {

    private Logger log = LogFactory.getLogger(LogType.JFast, ApiInvoker.class);
    /**
     * 目标Api实际类
     */
    private Class<?> apiClass;
    /**
     * 目标Api实际类对象
     */
    private Object apiObject;
    /**
     * 目标Api执行方法
     */
    private Method apiMethod;
    /**
     * HttpRequest
     */
    private HttpRequest request;
    /**
     * HttpResponse
     */
    private HttpResponse response;
    /**
     * Api方法对应参数类型数组
     */
    private Type[] paramTypes;
    /**
     * Api方法对应参数名数组
     */
    private String[] paramNames;
    /**
     * Api方法对应参数参数值
     */
    private Object[] arguments = new Object[0];
    /**
     * 日期类型格式化对象
     */
    private java.text.DateFormat dateFormat;
    /**
     * Api对应路径
     */
    private String apiRoute;
    /**
     * 实际访问路径
     */
    private String reqRoute;
    /**
     * 返回视图
     */
    private Object result;
    
    private boolean isPreLoged = false;
    
    private boolean isAfterLoged = false;
    
    private boolean argumentInit = false;
    
    private ApiInvocation invocation;
    
    private List<ValidateSet> validateSet = new ArrayList<ValidateSet>();
     
    
    public ApiInvocation getInvocation() {
		return invocation;
	}

	public void setInvocation(ApiInvocation invocation) {
		this.invocation = invocation;
	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public HttpRequest getRequest() {
		return request;
	}
	
	public void setRequest(HttpRequest request) {
		this.request = request;
	}

	public Type[] getParamTypes() {
		return paramTypes;
	}

	public void setParamTypes(Type[] paramTypes) {
		this.paramTypes = paramTypes;
	}

	public String[] getParamNames() {
		return paramNames;
	}

	public void setParamNames(String[] paramNames) {
		this.paramNames = paramNames;
	}

	public java.text.DateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(java.text.DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public boolean isLoged() {
		return isPreLoged;
	}

	public void setLoged(boolean isLoged) {
		this.isPreLoged = isLoged;
	}

	public Map<String, Object> getAopAttr() {
		return aopAttr;
	}

	public void setAopAttr(Map<String, Object> aopAttr) {
		this.aopAttr = aopAttr;
	}

	public Map<String, String> getDynamicReqRouteParamMap() {
		return dynamicReqRouteParamMap;
	}

	public void setDynamicReqRouteParamMap(
			Map<String, String> dynamicReqRouteParamMap) {
		this.dynamicReqRouteParamMap = dynamicReqRouteParamMap;
	}
	
	public String getParameter(String paramName){
		return dynamicReqRouteParamMap.get(paramName);
	}

	public Class<?> getApiClass() {
		return apiClass;
	}

	public Method getApiMethod() {
		return apiMethod;
	}

	public HttpResponse getResponse() {
		return response;
	}

	public String getApiRoute() {
		return apiRoute;
	}

	public String getReqRoute() {
		return reqRoute;
	}

	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}

	/**
     * invoke内置属性链，用于在拦截器中向Api传递参数
     */
    private Map<String, Object> aopAttr = new HashMap<String, Object>();

    /**
     * 请求路径对应的参数
     */
    private Map<String, String> dynamicReqRouteParamMap = new HashMap<String, String>();

    public void setApiRoute(String apiRoute) {
        this.apiRoute = apiRoute;
    }

    public void setReqRoute(String reqRoute) {
        this.reqRoute = reqRoute;
    }

    public void setApiClass(Class<?> apiClass) {
        this.apiClass = apiClass;
    }

    public void addAopAttr(String attrName, Object attrValue) {
        this.aopAttr.put(attrName, attrValue);
        if(null != paramNames)
	        for(int i = 0;i < paramNames.length; i++){ //如果验证器中调用，直接替换参数
	        	if(paramNames[i].equals(attrName)){
	        		arguments[i] = attrValue;
	        	}
	        }
    }

    public void setApiObject(Object apiObject) {
        this.apiObject = apiObject;
        if (apiObject != null) {
            this.apiClass = apiObject.getClass();
        }
    }

    public Object getApiObject() {
        return this.apiObject;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public void setApiMethod(Method apiMethod) {
        this.apiMethod = apiMethod;
    }

    /**
     * 获得请求参数对应的参数值
     *
     * @return
     * @throws NotFoundException
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Object[] getArguments() throws NotFoundException, ParseException, IllegalAccessException, InstantiationException, IOException, ClassNotFoundException {
        argumentInit = true;
    	ClassPool cp = ClassPool.getDefault();
        cp.insertClassPath(new ClassClassPath(ApiInvoker.class));
        CtClass clazz = cp.getCtClass(apiClass.getName());
        CtMethod method = clazz.getDeclaredMethod(apiMethod.getName());
        String paramName;
        Type paramType;
        paramTypes = apiMethod.getParameterTypes();
        paramNames = ClassUtils.getMethodParamNames(method);
        if(null == paramNames)
            paramNames = new String[paramTypes.length];
        arguments = new Object[paramTypes.length];
        Object[][] annotations = method.getAvailableParameterAnnotations();
        
        for (int i = 0; i < paramTypes.length; i++) {
            Param param = null;
            Remote ro = null;
            dateFormat = null;
            paramType = paramTypes[i];
            paramName = paramNames[i];
            if(null == paramName)
                paramName = "";
            //验证类型
            if (paramType == Date.class
                    || paramType == Date[].class
                    || paramType == Byte.TYPE
                    || paramType == Byte[].class
                    || paramType instanceof Collection)
                throw new IllegalArgumentException("不支持的参数类型:" + paramType);
            // 获取方法参数的属性
            for (Object anno : annotations[i]) {
                if (anno instanceof Param) {
                    param = (Param) anno;
                    if(StringUtils.isNotEmpty(param.name())){
                    	paramName = param.name();
                    	paramNames[i] = paramName;
                    }
                } else if (anno instanceof Remote) {
                    ro = (Remote) anno;
                    if (apiMethod.isAnnotationPresent(Get.class)
                            || apiMethod.isAnnotationPresent(Delete.class)
                            || apiMethod.isAnnotationPresent(Head.class)
                            || apiMethod.isAnnotationPresent(Connect.class)
                            || apiMethod.isAnnotationPresent(Copy.class)
                            || apiMethod.isAnnotationPresent(Trace.class)
                            || apiMethod.isAnnotationPresent(Mkcol.class)
                            || apiMethod.isAnnotationPresent(Move.class)
                            || apiMethod.isAnnotationPresent(Unlock.class)
                            || apiMethod.isAnnotationPresent(Options.class)) {
                        throw new IllegalArgumentException(apiMethod.getName()+"方法不可有@Remote注解，@Remote注解只适用于被ApiCaller调用的接口");
                    }
                }
            }
            //从拦截器中获取参数
            if (aopAttr.containsKey(paramName)) {
                arguments[i] = aopAttr.get(paramName);
                addValidate(param,paramName,arguments[i]);
                continue;
            }
            //从请求路径中取参数
            if (ObjectUtils.isNotNull(dynamicReqRouteParamMap.get(paramName))) {
                if (paramType == String.class) {
                    arguments[i] = dynamicReqRouteParamMap.get(paramName);
                } else if (paramType == Integer.TYPE || paramType == Integer.class) {
                    arguments[i] = Integer.parseInt(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Long.TYPE || paramType == Long.class) {
                    arguments[i] = Long.parseLong(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Float.TYPE || paramType == Float.class) {
                    arguments[i] = Float.parseFloat(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Double.TYPE || paramType == Double.class) {
                    arguments[i] = Double.parseDouble(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Short.TYPE || paramType == Short.class) {
                    arguments[i] = Short.parseShort(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Character.TYPE || paramType == Character.class) {
                	String chars = dynamicReqRouteParamMap.get(paramName);
                	if( null == chars || chars.equals(""))
                		arguments[i] = null;
                	else if( chars.length() > 1)
                		throw new IllegalArgumentException("参数 ["+paramName+"]长度超出范围");
                	else
                		arguments[i] = chars.toCharArray()[0];
                } else if (paramType == Short.TYPE || paramType == Short.class) {
                    arguments[i] = Short.parseShort(dynamicReqRouteParamMap.get(paramName));
                } else if (paramType == Boolean.TYPE || paramType == Boolean.class) {
                	arguments[i] = Boolean.parseBoolean(dynamicReqRouteParamMap.get(paramName));
                } else {
                    throw new IllegalArgumentException("路径参数不可以为" + paramType + "类型");
                }
                addValidate(param,paramName,arguments[i]);
                continue;
            }
            // 取远程调用传递的java对象参数
            if (ObjectUtils.isNotNull(ro) && request.isRemoteServiceRequest()) {
                if (request.getRequest().getInputStream().available() != 0) {
                    arguments[i] = new ObjectInputStream(request.getRequest().getInputStream()).readObject();
                    addValidate(param,paramName,arguments[i]);
                    continue;
                }
            }
            //从普通请求域获取参数
            if (paramType == HttpServletRequest.class) {
                arguments[i] = request.getRequest();
            } else if (paramType == HttpServletResponse.class) {
                arguments[i] = response;
            } else if (paramType == HttpSession.class) {
                arguments[i] = request.getRequest().getSession();
            } else if (paramType == Cookie[].class) {
                arguments[i] = request.getRequest().getCookies();
            } else {
                arguments[i] = getParamValue(paramType, paramName, param);
            }
            addValidate(param,paramName,arguments[i]);
        }
        return this.arguments;
    }
    
    public void addValidate(Param param,String paramName,Object paramValue) throws InstantiationException, IllegalAccessException{
    	if(ObjectUtils.isNotNull(param)){
            for(String validate:param.validate()){
            	if("".equals(validate))
            		continue;
            	ParameterValidate valid = ApiContext.getValidate(validate);
            	Assert.notNull(valid,String.format("找不到对应的验证器:%s",validate));
            	validateSet.add(new ValidateSet((ParameterValidate)ApiContext.fillResource(valid), paramName, paramValue));
            }
        }
    }
    /**
     * 获取复杂类型对象参数值
     *
     * @param paramType
     * @param paramName
     * @param param
     * @return
     * @throws IllegalAccessException
     * @throws ParseException
     * @throws InstantiationException
     */
    private Object getParamValue(Type paramType, String paramName,
                                 Param param) throws IllegalAccessException, ParseException, InstantiationException {
        String tempParamName = paramName;
        Object paramValue;
        if (ClassUtils.isCommonTypeOrWrapper(((Class<?>) paramType))
                || paramType == UploadFile.class) {
            paramValue = getPrimitiveValue(paramType, paramName, param);
        } else if(((Class<?>) paramType).isArray()){
            if (ObjectUtils.isNotNull(param) && param.isJson()) {
                paramValue = getJsonFieldValue(paramType, paramName, param);
            } else {
                paramValue = getArrayValue(paramType, paramName, param);
            }
        }else {
            paramValue = ((Class<?>) paramType).newInstance();
            //如果是JSON类型的参数
            if (ObjectUtils.isNotNull(param) && param.isJson()) {
                paramValue = getJsonFieldValue(paramType, paramName, param);
            } else {
                for (Field field : ReflectionUtils
                        .getFields((Class<?>) paramType)) {
                    field.setAccessible(true);
                    paramName = tempParamName + "." + field.getName();
                    paramType = field.getType();
                    Object value = getPrimitiveValue(paramType, paramName, param);
                    if (null != value)
                        field.set(paramValue, value);
                }
            }
        }
        return paramValue;
    }

    /**
     * 获得Json类型参数对应的参数值
     *
     * @param paramType
     * @param paramName
     * @param param
     * @return
     */
    private Object getJsonFieldValue(Type paramType, String paramName,
                                     Param param){
        Object value = null;
        JsonConfig config = new JsonConfig();
        if(ObjectUtils.isNotNull(param)){
        	config.registerJsonValueProcessor(java.util.Date.class, new JsonDateValueProcessor(param.datePattern()));
            JSONUtils.getMorpherRegistry().registerMorpher(new DateMorpher(new String[]{param.datePattern()}));
        }
        if(StringUtils.isNotEmpty(request.getParameter(paramName))) {
            if (((Class<?>) paramType).isArray()) {
                JSONArray jsonArray = JSONArray.fromObject(request.getParameter(paramName), config);
                Class<?> arrayInnerType = null;
				try {
					arrayInnerType = Class.forName(((Class<?>)paramType).getName().substring(2).replaceFirst(";", ""));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
                value = JSONArray.toArray(jsonArray,arrayInnerType);
            } else {
                JSONObject jsonObject = JSONObject.fromObject(request.getParameter(paramName), config);
                value = JSONObject.toBean(jsonObject,(Class<?>)paramType);
            }
        }
        return value;
    }

    /**
     * 获得集合数组类型参数对应的参数值
     *
     * @param paramType
     * @param paramName
     * @param param
     * @return
     */
    private Object getArrayValue(Type paramType, String paramName,
                                 Param param){
    	if(ObjectUtils.isNotNull(param))
    		dateFormat = new SimpleDateFormat(param.datePattern());
        Object value = null;
        String paramValues[] = request.getParameters(paramName);
        if (null != paramValues || !request.getFileMap().isEmpty()) {
            if (paramType == java.util.Date[].class) {
            	Assert.notNull(param, "参数" + paramName + "必须声明@Param且在@Param注解中注明日期解析模式");
                Assert.notEmpty(param.datePattern(), "参数 [" + paramName + " ]必须在@Param注解中注明日期解析模式");
                java.util.Date[] dates  = new java.util.Date[paramValues.length];
                for(int i= 0; i < paramValues.length; i++){
                    try {
                    	if(StringUtils.isNotEmpty(paramValues[i]))
                    		dates[i] = dateFormat.parse(paramValues[i]);
                    } catch (ParseException e) {
                        dates[i] = null;
                        throw new IllegalArgumentException("参数  ["+paramValues[i]+"] 不能被解析为日期：");
                    }
                }
                value = dates;
            } else if (paramType == UploadFile[].class) {
                Assert.isTrue(FileRequestResolver.isMultipart(request.getRequest()),
                        "上传文件时,表单属性enctype必须为multipart/form-data");
                value = request.getFileMap().values().toArray();
            } else if (paramType == Integer[].class) {
                value = StringUtils.strArr2IntArr(paramValues);
            } else if (paramType == Short[].class) {
                value = StringUtils.strArr2ShortArr(paramValues);
            } else if (paramType == Float[].class) {
                value = StringUtils.strArr2FloatArr(paramValues);
            } else if (paramType == Long[].class) {
                value = StringUtils.strArr2LongArr(paramValues);
            } else if (paramType == Double[].class) {
                value = StringUtils.strArr2DoubleArr(paramValues);
            } else if (paramType == String[].class) {
                value = paramValues;
            } else {
            	throw new IllegalArgumentException("不支持该参数类型："+paramType);
            }
        }
        return value;
    }

    /**
     * 获得简单类型参数对应参数值
     *
     * @param paramType
     * @param paramName
     * @param param
     * @return
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private Object getPrimitiveValue(Type paramType, String paramName,
                                     Param param) throws ParseException, IllegalAccessException, InstantiationException {
    	if(ObjectUtils.isNotNull(param))
    		dateFormat = new SimpleDateFormat(param.datePattern());
        Object value = null;
        if (null != request.getParameter(paramName)
                || hasExtraParam(paramName)
                || null != request.getUploadFile(paramName)) {
            String tempVal = request.getParameter(paramName);
            if(StringUtils.isEmpty(tempVal) && null == request.getUploadFile(paramName))
            	return null;
            if (paramType == Integer.TYPE || paramType == Integer.class) {
                value = Integer.parseInt(tempVal);
            } else if (paramType == Short.TYPE || paramType == Short.class) {
                value = Short.parseShort(tempVal);
            } else if (paramType == Float.TYPE || paramType == Float.class) {
                value = Float.parseFloat(tempVal);
            } else if (paramType == Long.TYPE || paramType == Long.class) {
                value = Long.parseLong(tempVal);
            } else if (paramType == Double.TYPE || paramType == Double.class) {
                value = Double.parseDouble(tempVal);
            } else if (paramType == Boolean.TYPE || paramType == Boolean.class) {
                value = Boolean.parseBoolean(tempVal);
            } else if (paramType == Character.TYPE || paramType == Character.class) {
            	if( null == tempVal || tempVal.equals("") )
            		value = null;
            	else if( tempVal.length() > 1)
            		throw new IllegalArgumentException("参数 ["+paramName+"]长度超出范围");
            	else
            		value = tempVal.toCharArray()[0];
            } else if (paramType == String.class) {
                value = tempVal;
            } else if (paramType == UploadFile.class) {
                Assert.isTrue(FileRequestResolver.isMultipart(request.getRequest()),
                        "上传文件时,表单属性enctype必须为multipart/form-data.");
                value = request.getUploadFile(paramName);
            } else if (paramType == Date.class || paramType == java.util.Date.class) {
            	Assert.notNull(param, "参数" + paramName + "必须声明@Param且在@Param注解中注明日期解析模式");
                Assert.notEmpty(param.datePattern(), "参数 [" + paramName + " ]必须在@Param注解中注明日期解析模式");
                if(StringUtils.isNotEmpty(tempVal))
                	try{
                		value = dateFormat.parse(tempVal);
                	} catch(ParseException e){
                		throw new IllegalArgumentException("参数  ["+tempVal+"] 不能被解析为日期：");
                	}
            } else {
            	throw new IllegalArgumentException("不支持该参数类型："+paramType);
            }
        }
        return value;
    }

    /**
     * 判断是否有对应子参数名
     *
     * @param paramName
     * @return
     */
    private boolean hasExtraParam(String paramName) {
        boolean bool = false;
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements())
            if (paramNames.nextElement().indexOf(paramName) != -1) {
                bool = true;
                break;
            }
        return bool;
    }

    /**
     * Api对应方法执行操作
     *
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws NotFoundException
     * @throws ParseException
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws InstantiationException
     */
    public Object invoke() throws InvocationTargetException,
            IllegalAccessException,
            NotFoundException, ParseException, ClassNotFoundException, IOException, InstantiationException {
        ReflectionUtils.makeAccessible(apiMethod); 
        preLog();
        validate();
        result = apiMethod.invoke(apiObject, arguments);
        afterLog();
        return result;
    }

    private void validate() {
		for(ValidateSet validate:validateSet){
			try {
				validate.validate.validate(invocation, validate.paramName, validate.paramValue);
			} catch (Exception e) {
				this.invocation.addException(e);
				validate.validate.errorHandler(invocation, e);
				break;
			}
		}
	}

	public void preLog() throws NotFoundException, ParseException, IllegalAccessException, InstantiationException, IOException, ClassNotFoundException {
    	synchronized (this) {
	    	if(!isPreLoged){
	    		if(!argumentInit){
	    			try{
	    				getArguments();
	    			} catch(IllegalArgumentException e){
	    				throw e;
	    			} finally {
	    				log.info("\n%s	:	------------------ request message ------------------\n" +
		                        "%s	:	%s\n" +
		                        "%s	:	%s\n" +
		                        "%s	:	%s\n" +
		                        "%s	:	%s\n" +
		                        "%s	:	%s\n" +
		                        "%s	:	%s\n",
		                "receive request",
		                "request uri", request.getRequestURI(),
		                "request method", isAjax(request) ? request.getMethod() + "( " + request.getHeader("X-Requested-With") + " )" : request.getMethod(),
		                "mapped path", apiRoute,
		                "target class", apiObject.getClass().getSimpleName() + "." + apiMethod.getName() + "()",
		                "param names", "[" + StringUtils.join(paramNames, ",") + "]",
		                "param values", "[" + StringUtils.join(arguments, ",") + "]");
	    				isPreLoged = true;
	    			}
	    		} 
	    	}
    	}
    }

    public void afterLog() {
    	afterLog(result);
    }
    
    public void afterLog(Object view) {
    	synchronized (this) {
    		if(!isAfterLoged){
		    	log.info("\n%s	:	------------------ response message ------------------\n" +
		                        "%s	:	%s\n",
		             "send response",
		             "response view", null == view ? "" : view.toString());
		    	isAfterLoged = true;
    		}
    	}
    }

    /**
     * 获得远程IP地址
     *
     * @param request
     * @return
     */
    public static String getIpAddr(HttpRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 获得Ajax请求描述
     *
     * @param request
     * @return
     */
    public static boolean isAjax(HttpRequest request) {
        return null != request.getHeader("X-Requested-With");
    }
    
    public void init(){
    	//请求路径和Api标志路径不一致时，处理动态路径参数
        if (!apiRoute.equals(reqRoute)) {
            String[] dynamicPaths = apiRoute.split("/");
            String[] reqPaths = reqRoute.split("/");
            for (int i = 0; i < dynamicPaths.length; i++) {
                if (dynamicPaths[i].startsWith(":")) {
                    dynamicReqRouteParamMap.put(dynamicPaths[i].substring(1), reqPaths[i]);
                }
            }
        }
    }
    
    class ValidateSet{	
    	ParameterValidate validate;
    	String paramName;
    	Object paramValue;
    	
    	ValidateSet(ParameterValidate validate,String paramName,Object paramValue){
    		this.validate = validate;
    		this.paramName = paramName;
    		this.paramValue = paramValue;
    	}
    }
}
