/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

import java.io.Serializable;

/**
 * 服务回调视图
 */
public class CallerResponse extends View{

    private Object view;

    public CallerResponse(Object obj){
        if((obj instanceof Serializable))
            this.view = obj;
        else
            throw new IllegalArgumentException("服务回调视图参数必须是可序列化对象,对象需要实现Serializable接口");
    }

    @Override
    public Object getView() {
        return this.view;
    }

    @Override
    public String toString() {
        return "serializable response object:" + this.getView();
    }
}
