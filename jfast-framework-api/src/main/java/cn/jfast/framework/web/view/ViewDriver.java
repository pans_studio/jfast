/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import cn.jfast.framework.base.util.Assert;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.base.prop.SDKProp;
import cn.jfast.framework.web.api.Api;
import cn.jfast.framework.web.api.HttpRequest;
import cn.jfast.framework.web.api.ApiInvocation;
import cn.jfast.framework.web.api.ApiContext;
import cn.jfast.framework.web.api.HttpResponse;
import cn.jfast.framework.web.view.viewtype.*;

/**
 * 视图驱动
 */
public class ViewDriver {

    private Logger log = LogFactory.getLogger(LogType.JFast, ViewDriver.class);

    private List<Exception> expList = new ArrayList<Exception>();

    private final String APPLICATION_OBJECT = "application/x-java-serialized-object";

    private final String TEXT_HTML = "text/html;charset=";

    private final String TEXT_PLAIN = "text/plain; charset=";

    private final String APPLICATION_DOWNLOAD = "application/x-msdownload";

    private final String APPLICATION_JSON = "application/json; charset=";

    private final String CONTENT_DISPOSITION = "Content-Disposition";

    private final String CONTENT_LENGTH = "Content-Length";
    
    private boolean isRendered = false;

    public synchronized void render(Object view, HttpRequest request,
    		HttpResponse response, List<Exception> ex) {
    	if(isRendered)  // 
    		return;
        if (null == ex || ex.isEmpty()) {
            if(null != view) {
                if (view instanceof Jsp)
                    this.renderJSP((Jsp) view, request, response);
                else if (view instanceof Text)
                    this.renderText((Text) view, request, response);
                else if (view instanceof Html)
                    this.renderHtml((Html) view, request, response);
                else if (view instanceof Json)
                    this.renderJSON((Json) view, request, response);
                else if (view instanceof Download)
                    this.renderDownload((Download) view, request, response);
                else if (view instanceof cn.jfast.framework.web.view.viewtype.Api)
                    this.renderService((cn.jfast.framework.web.view.viewtype.Api) view, request, response);
                else if (view instanceof String)
                    this.renderJSP(new Jsp((String) view), request, response);
                else if (view instanceof CallerResponse)
                    this.renderCallback((CallerResponse)view, request, response);
                else
                    this.renderNull();
            }
        } else {
            try {
                for(Exception e:ex){
                    e.printStackTrace();
                }
                if (SDKProp.DEVMODE) {
                    String expHtml = "";
                    response.reset();
                    response.setContentType(TEXT_HTML + SDKProp.ENCODING);
                    for(Exception e:ex){
                        expHtml += generateException(e);
                    }
                    response.getWriter().println(expHtml);
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        }
        isRendered = true;
    }

    private void renderHtml(Html view, HttpRequest request, HttpResponse response) {
        try {
            request.getRequestDispatcher(
                    "/" + view.getView()).forward(
                    request, response);
        }catch (IOException e) {
            log.error(e.getMessage());
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    private void renderCallback(CallerResponse view, HttpRequest request, HttpResponse response) {
        ObjectOutputStream os = null;
        try {
            response.setCharacterEncoding(SDKProp.ENCODING);
            response.setContentType(APPLICATION_OBJECT);
            os = new ObjectOutputStream(response.getOutputStream());
            os.writeObject(view.getView());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void renderText(Text view, HttpRequest request, HttpResponse response) {
        try {
            response.setCharacterEncoding(SDKProp.ENCODING);
            response.setContentType(TEXT_PLAIN + SDKProp.ENCODING);
            response.getWriter().println(view.getView());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void renderNull() {}

    private void renderService(cn.jfast.framework.web.view.viewtype.Api view, HttpRequest request,
    		HttpResponse response) {
        for (Entry<String, String> requestAttr : view.getActionAttrs().entrySet()) {
            request.setAttribute(requestAttr.getKey(), requestAttr.getValue());
        }
        Api action = ApiContext.getApi(view.getView() + "/$" + request.getMethod().toLowerCase() + "$");
        Assert.notNull(action, "找不到对应API [" + view.getView() + "/$" + request.getMethod().toLowerCase() + "$]");
        try {
            new ApiInvocation(action, action.getApi().newInstance(), (HttpRequest)request, response, view.getView() + "/$" + request.getMethod().toLowerCase() + "$").invoke();
        } catch (InstantiationException e) {
            expList.add(e);
            this.render(view,request,response,expList);
        } catch (IllegalAccessException e) {
            expList.add(e);
            this.render(view,request,response,expList);
        }
    }

    private void renderDownload(Download view, HttpRequest request,
                                HttpResponse response) {
        ServletOutputStream out = null;
        try {
            response.setCharacterEncoding(SDKProp.ENCODING);
            String fileName = response.encodeURL(new String(view.getFilename().getBytes(), SDKProp.ENCODING));
            if (view.getMode() == DownloadMode.OPTION) {
                response.setContentType(APPLICATION_DOWNLOAD);
                response.setHeader(CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
            } else {
                response.setContentType(view.getContentType());
                response.setHeader(CONTENT_DISPOSITION, "inline; filename=\"" + fileName + "\"");
            }
            response.addHeader(CONTENT_LENGTH, "" + view.getFileData().length);
            out = response.getOutputStream();
            out.write(view.getFileData());
            response.setStatus(HttpResponse.SC_OK);
            response.flushBuffer();
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void renderJSP(Jsp view, HttpRequest request,
                           HttpResponse response) {

        try {
            if (view.getRoute() == Route.REDIRECT) {
                response.setCharacterEncoding(SDKProp.ENCODING);
                response.setContentType(TEXT_HTML + SDKProp.ENCODING);
                response.sendRedirect(request.getContextPath()
                        + "/" + view.getView());
            } else {
                for (Entry<String, Object> requestAttr : view.getRequestAttrs().entrySet()) {
                    request.setAttribute(requestAttr.getKey(), requestAttr.getValue());
                }
                request.getRequestDispatcher(
                        "/" + view.getView()).forward(
                        request, response);
            }
        } catch (ServletException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }

    private void renderJSON(Json view, HttpRequest request,
                            HttpResponse response) {
        try {
            response.setCharacterEncoding(SDKProp.ENCODING);
            response.setContentType(APPLICATION_JSON + SDKProp.ENCODING);
            response.getWriter().println(view.getView());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private String generateException(Exception ex) {
        Throwable t = ex.getCause();
        StringBuffer sbf = new StringBuffer("<p style='font-family:Micro Yahei;font-size:12px;padding-left:20px;color:red;'>");
        sbf.append(ex.toString() + "<br>");
        StackTraceElement[] exs = ex.getStackTrace();
        for (StackTraceElement es : exs) {
            sbf.append("at " + es.getClassName() + "." + es.getMethodName() + "(" + es.getFileName() + ":" + es.getLineNumber() + ")<br>");
        }
        if (null != t) {
            sbf.append("<b>Cause By</b> : " + t.toString() + "<br>");
            StackTraceElement[] txs = t.getStackTrace();
            for (StackTraceElement es : txs) {
                sbf.append("at " + es.getClassName() + "." + es.getMethodName() + "(" + es.getFileName() + ":" + es.getLineNumber() + ")<br>");
            }
            sbf.append("</p>");
        }
        return sbf.toString();
    }
}
