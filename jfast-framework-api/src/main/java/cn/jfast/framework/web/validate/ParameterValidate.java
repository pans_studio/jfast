package cn.jfast.framework.web.validate;

import cn.jfast.framework.web.api.ApiInvocation;


public interface ParameterValidate {
	
	void validate(ApiInvocation invoke, String paramName, Object value) throws Exception;
	
	void errorHandler(ApiInvocation invoke, Exception e);
	
}
