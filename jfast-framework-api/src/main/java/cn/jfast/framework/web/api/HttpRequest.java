/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.api;

import cn.jfast.framework.base.prop.SDKProp;
import cn.jfast.framework.upload.FileRequest;
import cn.jfast.framework.upload.FileRequestResolver;
import cn.jfast.framework.upload.UploadFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Api 综合请求对象
 */
public class HttpRequest extends HttpServletRequestWrapper {

    /** HttpServletRequest请求 */
    private HttpServletRequest request;
    /** form-data 请求 */
    private FileRequest fileRequest;
    /** PUT请求参数 */
    private Map<String,String> parameterMap = new HashMap<String,String>();
    /** 文件上传解析器 */
    private FileRequestResolver resolver;

    public HttpRequest(HttpServletRequest request) throws IOException {
        super(request);
        this.request = request;
        resolver = new FileRequestResolver();
        fileRequest = resolver.resolveMultipart(request);
        if(request.getMethod().equals("PUT")){
        	BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));  
        	String line;  
            if((line = in.readLine()) != null){
            	String[] paramKv = line.split("&");
            	String[] kv;
            	for(String param:paramKv){
            		kv = param.split("=");
            		if(parameterMap.containsKey(kv[0])) {
            			if(kv.length == 1)
            				parameterMap.put(kv[0], parameterMap.get(kv[0])+"&"+"");
            			else
            				parameterMap.put(kv[0], parameterMap.get(kv[0])+"&"+URLDecoder.decode(kv[1],SDKProp.ENCODING));
            		} else {
            			if(kv.length == 1)
            				parameterMap.put(kv[0], "");
            			else
            				parameterMap.put(kv[0], URLDecoder.decode(kv[1],SDKProp.ENCODING));
            		}
            	}
            }  	
        }
    }

    /**
     * 获得HttpServletRequest请求
     * @return
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * 获得指定名称的上传文件
     * @param fieldName
     * @return
     */
    public UploadFile getUploadFile(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getFileMap().get(fieldName);
        else
            return null;
    }

    /**
     * 获得指定名称的参数值
     * @param fieldName
     * @return
     */
    public String getParameter(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttr(fieldName);
        else if(request.getMethod().equals("PUT"))
            return parameterMap.get(fieldName);
        else
        	return request.getParameter(fieldName);
    }

    /**
     * 获得数组型参数
     * @param fieldName
     * @return
     */
    public String[] getParameters(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttrMap().get(fieldName);
        else if(request.getMethod().equals("PUT"))
        	 return parameterMap.get(fieldName).split("&");
        else
            return request.getParameterValues(fieldName);
    }

    /**
     * 获得上传文件的Map链
     * @return
     */
    public Map<String, UploadFile> getFileMap() {
        return this.fileRequest.getFileMap();
    }

    /**
     * 获得请求域所有参数名
     * @return
     */
    public Enumeration<String> getParameterNames() {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttrNames();
        else
            return this.request.getParameterNames();
    }

    /**
     * 判断是否是远程服务调用请求
     * @return
     */
    public boolean isRemoteServiceRequest() {
        boolean bool = false;
        try {
            bool = request.getInputStream().available() != 0 && !FileRequestResolver.isMultipart(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bool;
    }
}
