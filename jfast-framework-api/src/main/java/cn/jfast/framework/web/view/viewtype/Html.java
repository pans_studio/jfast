package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

/**
 * 返回指定Html页面
 */
public class Html extends View {

    private String view;

    public Html(String view){
        if(!view.endsWith(".html"))
            view += ".html";
        this.view = view;
    }

    @Override
    public Object getView() {
        return this.view;
    }

    @Override
    public String toString() {
        return "html:"+this.getView();
    }
}
