/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.schedule;

import java.lang.reflect.Method;

/**
 * 定时任务封装类
 */
public class ScheduledJob {
    private Object Object;
    private Method method;
    private String cron;
    private long delay;
    private int repeat;
    private long repeatInterval;

    public ScheduledJob(Object Object, Method method, String cron, long delay, int repeat, long repeatInterval) {
        this.Object = Object;
        this.method = method;
        this.cron = cron;
        this.delay = delay;
        this.repeat = repeat;
        this.repeatInterval = repeatInterval;
    }

    public Object getSchedule() {
        return Object;
    }

    public void setSchedule(Object Object) {
        this.Object = Object;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }

    public long getRepeatInterval() {
        return repeatInterval;
    }

    public void setRepeatInterval(long repeatInterval) {
        this.repeatInterval = repeatInterval;
    }

}
