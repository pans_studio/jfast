<h6>jfast是一款restful mvc框架.可以做普通web项目，也可用作远程服务端，支持跨域<h6>

<p>
Api类（控制器 == Struts的Action == Spring MVC的Controller）可接收参数：<br>
     1. java基本数据类型<br>
     2. 数组(注意：不支持集合参数 )<br>
     3. java对象<br>
     &nbsp;&nbsp;前台或者远程客户端怎么向jfast中传递对象：<br>
     &nbsp;&nbsp;比如 对象User 有 username和password属性<br>
     &nbsp;&nbsp; &nbsp;&nbsp;1.json描述对象结构<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;user = {username:" ",password:" "}<br>
     &nbsp;&nbsp; &nbsp;&nbsp;2.和struts类似:<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;那么传递<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;user.username = " "<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;user.password = " ",<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;后台会自动转为User对象<br>
     &nbsp;&nbsp; &nbsp;&nbsp;3.远程传递<br>
     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;远程客户端通过二进制流传递java可序列化对象<br>
</p>
<p>
数据层
     Dao层只需要写好方法声明，不需要具体实现,jfast 根据方法描述,自动生成SQL，返回指定的结果
     同时 jfast数据层最大亮点是：<br>
     ** jfast可以与其他任何ORM框架相融：同一个dao方法，可以同时是jfast式Dao操作，也可以是其他框架的Dao操作，不同的ORM共享同一个方法 **
</p>
完整的 Restful设计

Model层 封装了常用的返回<br>
    返回Json,Jsp,Html,文件下载,返回远程客户端的java对象,Api(内部调用其他Api)

定时任务<br>
    创建定时任务特别简单，不需要额外的任何配置，不需要任何依赖，就是一个普通的简单的java类

** 注意 **<br>
    使用jfast时，尽量使用eclipse工程，在intellj环境下 无法识别Api和Dao方法参数名，需要在每个参数上加@Param注解

